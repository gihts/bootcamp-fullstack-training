# Agenda

1. `git pull`
1. What is an algorithm?
1. What is algorithm complexity?
1. What is Big-O notation?
1. Week 17 Activity 21: What is a linear search?
1. Week 17 Activity 22: Implement a linear search
1. Review Week 17 Activity 22
1. Week 17 Activity 23: What is recursion?
1. Week 17 Activity 24: Analyze the code
1. Review Week 17 Activity 24
1. Week 17 Activity 25: Binary search
1. Week 17 Activity 26: Fix the binary search
1. Review Week 17 Activity 26
1. Week 17 Activity 27: Shell scripts
1. Week 17 Activity 28: Interview practice with sorting algorthims
1. Homework 17: RegEx gist (optional)
1. [Install MongoDB!](https://coding-boot-camp.github.io/full-stack/mongodb/how-to-install-mongodb)
