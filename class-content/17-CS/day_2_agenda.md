# Agenda

1. Project 2 Surveys - Thank you!
1. Next week: some staff outages
1. `git pull`
1. Inheritance vs Composition
1. Week 17 Activity 11: What is a higher-order function?
1. Week 17 Activity 12: Use the higher-order function `.reduce`
1. Review Week 17 Activity 12
1. Week 17 Activity 13: What is a closure?
1. Week 17 Activity 14: Create a counter using a closure
1. Review Week 17 Activity 14
1. Week 17 Activity 15: What is a factory function?
1. Week 17 Activity 16: Fix the Student factory
1. Review Week 17 Activity 16
1. Week 17 Activity 17: Inheritance vs. Composition
1. Week 17 Activity 18: Refactor a Lesson class to use Composition
1. Review Week 17 Activity 18
1. Week 17 Activity 19: Event Delegation
1. Week 17 Activity 20: Analyze the event delegation code
1. Review Week 17 Activity 20
