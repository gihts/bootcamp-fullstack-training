// action
const getInformation = (state) => ({
  information: () => console.log(state.title, state.description),
});

const getGrades = (state) => ({
  grades: () => console.log("Here are the grades"),
});

const lesson = (title, description) => {
  const state = {
    title,
    description,
  };
  return {
    ...getInformation(state),
    ...getGrades(state),
  };
};

const csForJS = lesson("Unit 17 - Computer Science", "CS for JS");

csForJS.information();
csForJS.grades();

console.dir(csForJS);
