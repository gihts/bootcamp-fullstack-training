const fetch = require("node-fetch");
require("dotenv").config();

function fetchData(cityname) {
  const username = "John";

  fetch(
    `https://api.openweathermap.org/data/2.5/weather?q=${cityname}&appid=${process.env.APIKEY}`,
    {
      method: "GET",
    }
  )
    .then((res) => res.json())
    .then((res) => {
      // this inner function still has access to the variable cityname
      // even after the outer function fetchData has already completed
      // execution
      console.log(`${cityname} is available because of closures`, res);
    });
}

module.exports = fetchData;
