# Agenda

1. `git pull`
1. Review Model-View-Controller (MVC)
1. Week 14 Activity 9 - Separation of concerns
1. Week 14 Activity 10 - Analyze the code
1. Review Week 14 Activity 10
1. Week 14 Activity 11 - Handlebars partials
1. Week 14 Activity 12 - Create a painting partial
1. Review Week 14 Activity 12
1. Week 14 Activity 15 - express sessions
1. Week 14 Activity 16 - add sessions to login route
1. Review Week 14 Activity 16
1. Week 14 Activity 17 - Cookies!
1. Week 14 Activity 18 - explain the cookies
