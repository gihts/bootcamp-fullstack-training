# Agenda

1. `git pull`
1. Review Week 14 Activity 18 - securing cookies
1. Week 14 Activity 19 - Limiting auth access to route
1. Week 14 Activity 20 - Walk through Express middleware
1. Week 14 Activity 21 - Review MVC
1. Week 14 Activity 22 - Render Data into Handlebars files
1. Review Week 14 Activity 22
1. Week 14 Activity 23 - Review Authentication
1. Week 14 Activity 24 - Analyze the code
1. Review Week 14 Activity 24
1. Week 13 Activity 27 - Linting
1. Week 14 Activity 28 - Mini Project
1. Homework 14: Tech Blog
