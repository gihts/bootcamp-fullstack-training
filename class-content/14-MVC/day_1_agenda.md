# Agenda

1. `git pull`
1. Intro to Model-View-Controller (MVC)
1. Week 14 Activity 1 - MVC example
1. Week 14 Activity 2 - Study express-handlebars MVC
1. Review Week 14 Activity 2
1. Week 14 Activity 3 - Handlebars expressions
1. Week 14 Activity 4 - Modify the Handlebars expressions
1. Review Week 14 Activity 4
1. Week 14 Activity 5 - Handlebars helpers (looping)
1. Week 14 Activity 6 - Handlebars helpers (if/else)
1. Review Week 14 Activity 6
1. Week 14 Activity 7 - Feeding database data to handlebars
1. Week 14 Activity 8 - Render the list of dishes
1. Review Week 14 Activity 8
1. Week 14 Activity 9 - Separation of concerns
1. Week 14 Activity 10 - Analyze the code
1. Review Week 14 Activity 10
