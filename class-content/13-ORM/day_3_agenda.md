# Agenda

1. `git pull`
1. Week 13 Activity 15: bcrypt: hashing passwords
1. Week 13 Activity 16: Analyze the code
1. Review Week 13 Activity 16
1. Week 13 Activity 21: One-to-One Associations
1. Week 13 Activity 22: Analyze the One-to-One association
1. Review Week 13 Activity 22
1. Week 13 Activity 23: One-to-Many Associations
1. Week 13 Activity 24: Create a One-to-Many association
1. Review Week 13 Activity 24
1. Week 13 Activity 28: Mini Project
1. Week 13 Homework: E-Commerce Back End
