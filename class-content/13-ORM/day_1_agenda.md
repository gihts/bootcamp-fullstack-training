# Agenda

1. `git pull`
1. Backtrack: Week 9 Activity 25 Object Destructuring
1. Week 9 Activity 26: Destructure some objects
1. Introduction to ORM's: `sequelize`
1. This week's Mini-Project: Travel Planner
1. Week 13 Activity 1: Installing Sequelize
1. Week 13 Activity 2: use Dotenv
1. Review Week 13 Activity 2
1. Week 13 Activity 3: Sequelize Models
1. Week 13 Activity 4: Sequelize Model options
1. Review Week 13 Activity 4
1. Week 13 Activity 5: Creating items
1. Week 13 Activity 6: Analyze the code
1. Review Week 13 Activity 6
