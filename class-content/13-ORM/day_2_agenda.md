# Agenda

1. `git pull`
1. Week 13 Activity 7: Updating and Deleting by ISBN
1. Week 13 Activity 8: update and delete books by ID
1. Review Week 13 Activity 8
1. Week 13 Activity 9: `async/await`
1. Week 13 Activity 10: Change Promises into `async/await`
1. Review Week 13 Activity 10
1. Week 13 Activity 11: Using `try...catch` with `async/await`
1. Week 13 Activity 12: Convert `.catch` to `try...catch`
1. Review Week 13 Activity 12
1. Week 13 Activity 13: Using sequelize validation
1. Week 13 Activity 14: Add validation to the User model
1. Review Week 13 Activity 14
1. Week 13 Activity 15: How to secure a password
1. Week 13 Activity 16: Analyze the code
1. Review Week 13 Activity 16
1. Week 13 Activity 17: Sequelize Hooks
1. Week 13 Activity 18: Put the password hashing in the hook
1. Review Week 13 Activity 18
1. Week 13 Activity 19: Instance Methods: extending the model
1. Week 13 Activity 20: Add a check password method to the User model
1. Review Week 13 Activity 20
