const { UserInputError } = require("apollo-server-express");
const { Matchup, Tech } = require("../models");

const resolvers = {
  Query: {
    matchups: async () => {
      console.log("Getting matchups");
      return await Matchup.find({});
    },
    matchup: async (parent, { id }) => {
      console.log("Getting matchup: ", id);
      return await Matchup.findOne({ _id: id });
    },
    techs: async () => {
      console.log("Getting techs");
      return await Tech.find({});
    },
  },

  Mutation: {
    createMatchup: async (parent, { tech1, tech2 }) => {
      console.log("Creating matchup");
      return await Matchup.create({ tech1, tech2 });
    },
    createVote: async (parent, { id, techNum }) => {
      console.log("Creating vote");
      const vote = await Matchup.findOneAndUpdate(
        { _id: id },
        { $inc: { [`tech${techNum}_votes`]: 1 } },
        { new: true }
      );

      if (!vote) {
        throw new UserInputError("Couldn't find that matchup");
      }

      return vote;
    },
  },
};

module.exports = resolvers;
