import { gql } from "@apollo/client";

export const QUERY_TECHS = gql`
  query getTechs {
    techs {
      _id
      name
    }
  }
`;

export const QUERY_MATCHUPS = gql`
  query matchups {
    matchups {
      _id
      tech1
      tech2
      tech1_votes
      tech2_votes
    }
  }
`;

export const QUERY_MATCHUP_SINGLE = gql`
  query matchup($id: ID!) {
    matchup(id: $id) {
      _id
      tech1
      tech2
      tech1_votes
      tech2_votes
    }
  }
`;
