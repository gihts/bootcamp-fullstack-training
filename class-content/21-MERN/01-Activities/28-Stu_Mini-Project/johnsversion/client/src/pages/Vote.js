import React from "react";
import { useQuery, useMutation } from "@apollo/client";
import { useParams, Link } from "react-router-dom";

import { QUERY_MATCHUP_SINGLE } from "../utils/queries";
import { CREATE_VOTE } from "../utils/mutations";

const Vote = () => {
  console.log("Rendering Vote...");
  let { id } = useParams();
  const {
    loading,
    data,
    error: matchupError,
  } = useQuery(QUERY_MATCHUP_SINGLE, {
    variables: { id },
  });
  let matchup = data?.matchup;
  console.log("Rendering Vote: loading?", loading);
  console.log("Rendering Vote: got matchup?", data);
  console.log("Rendering Vote: error?", matchupError);

  const [createVote, { error, data: createData, loading: createLoading }] =
    useMutation(CREATE_VOTE);

  const handleVote = async (techNum) => {
    try {
      console.log("Handling Vote: createData", createData);
      console.log("Handling Vote: createLoading", createLoading);
      await createVote({ variables: { id, techNum } });
      console.log("Handled Vote: createData", createData);
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <div className="card bg-white card-rounded w-50">
      {matchupError && <div>{JSON.stringify(matchupError, null, 2)}</div>}
      {loading ? (
        <div>Loading...</div>
      ) : (
        <React.Fragment>
          <div className="card-header bg-dark text-center">
            <h1>Here is the matchup!</h1>
          </div>
          <div className="card-body text-center mt-3">
            <h2>
              {matchup.tech1} vs. {matchup.tech2}
            </h2>
            <h3>
              {matchup.tech1_votes} : {matchup.tech2_votes}
            </h3>
            <button className="btn btn-info" onClick={() => handleVote(1)}>
              Vote for {matchup.tech1}
            </button>{" "}
            <button className="btn btn-info" onClick={() => handleVote(2)}>
              Vote for {matchup.tech2}
            </button>
            <div className="card-footer text-center m-3">
              <br></br>
              <Link to="/">
                <button className="btn btn-lg btn-danger">
                  View all matchups
                </button>
              </Link>
            </div>
          </div>
        </React.Fragment>
      )}
      {error && <pre>{JSON.stringify(error, null, 2)}</pre>}
    </div>
  );
};

export default Vote;
