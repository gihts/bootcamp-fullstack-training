import React from "react";
import { useQuery } from "@apollo/client";
import { Link } from "react-router-dom";

import { QUERY_MATCHUPS } from "../utils/queries";

const Home = () => {
  console.log("Rendering Home...");
  const { loading, data } = useQuery(QUERY_MATCHUPS);
  const matchupList = data?.matchups || [];

  console.log("Rendering Home: got matchups?", data);

  return (
    <div className="card bg-white card-rounded w-50">
      <div className="card-header bg-dark text-center">
        <h1>Welcome to Tech Matchup!</h1>
      </div>
      {loading ? (
        <div>Loading...</div>
      ) : (
        <React.Fragment>
          <div className="card-body m-5">
            {matchupList.length > 0 ? (
              <React.Fragment>
                <h2>Here is a list of matchups you can vote on:</h2>
                <ul className="square">
                  {matchupList.map((matchup) => {
                    return (
                      <li key={matchup._id}>
                        <Link to={{ pathname: `/matchup/${matchup._id}` }}>
                          {matchup.tech1} vs. {matchup.tech2}
                        </Link>
                      </li>
                    );
                  })}
                </ul>
              </React.Fragment>
            ) : (
              <h2 className="text-center">No matchups found</h2>
            )}
          </div>
          <div className="card-footer text-center m-3">
            <h2>Ready to create a new matchup?</h2>
            <Link to="/matchup">
              <button className="btn btn-lg btn-danger">Create Matchup!</button>
            </Link>
          </div>
        </React.Fragment>
      )}
    </div>
  );
};

export default Home;
