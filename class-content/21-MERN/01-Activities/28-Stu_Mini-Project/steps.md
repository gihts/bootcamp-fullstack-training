# Steps to complete

Here are the general steps you need to complete this activity

## Server

### Add the Apollo Server

1. Install the needed package dependencies.
   - What packages are needed to add an Apollo Server?
   - Note: make sure you are using `apollo-express-server` version 2.
   - How does one install the specific version of an `npm` package?
   - Make sure you are in the `server` folder when you `npm i`
1. Create a `schemas` folder.
   - What files go into a `schemas` folder?
1. Add a `typedefs.js`, a `resolvers.js` and an `index.js` file.
1. Add the code to the `index.js` file.
   - How have we done this in previous activities?
   - Ask yourself: why are we doing this? what are the advantages?
1. Add the type definitions to `typedefs.js`.

   - How is a `typedefs` file created?
   - What dependencies are needed? (look in the previous activities)
   - Add a type for each mongoose model
     - What are my `models`?
   - Add a `Query` type
     - Look in the `controllers` folder. In your `Query` type, add an entry point for each `GET` route in your `controllers`.
       - What arguments are needed for each entry point? What are their types (e.g., `String`, `ID`, `Int`)
   - Add a `Mutation` type
     - Look in the `controllers` folder. In your `Mutation` type, add an entry point for each `POST`, `PUT` or `DELETE` route in your `controllers`.
     - What arguments are needed for each entry point? What are their types (e.g., `String`, `ID`, `Int`)

1. Add the resolver functions to the `resolvers.js` file
   - Create a `resolvers` object using an example from previous activities.
     - You will need a `Query` property and a `Mutation` property.
   - Add a function for weach entry point defined in your `Query` type to the `Query` property of your resolver.
     - What is the call signature of a resolver? I.e., what arguments and in what order are they passed into the resolver functions?
   - Add a function for weach entry point defined in your `Mutation` type to the `Mutation` property of your resolver.
     - What is the call signature of a resolver? I.e., what arguments and in what order are they passed into the resolver functions?
1. Add an Apollo server to `server.js`.
   - How have we been doing this in the previous activities?

### Remove the Express RESTful routes

- [What is REST](https://restfulapi.net/)

1. Remove the `controllers` folder
1. Remove the references to the `routes` from `server.js`
1. Add a `*` `GET` route to `server.js` that directs to `../client/build/index.html`
   - How has this been done in previous activities?
   - What is the purpose of this route?

## Client

1. Install any dependencies
   - what are the required packages?
   - Make sure you are in the `client` folder when you `npm i`
1. Create an `ApolloClient` in `App.js`
   - Do we need to add a context? (i.e., are we requiring authentication?)
   - Make sure you supply the client in an `ApolloProvider`
1. Create a `queries.js` file and a `mutations.js` file in `utils`
1. Look at `utils/api.js`:
   - Make a GraphQL query for each `GET` fetch in the `queries.js` file
   - Make a GraphQL query for each `POST`, `PUT` and `DELETE` fetch in the `mutations.js` file.
   - Use previous activities as your guide.
1. In the `pages`, look for when `useEffect` as used to do API calls
   - Use `useQuery` when getting items.
   - Use `useMutation` when creating or changing items
     - for this you won't have to update the cache
   - Remove the `useEffect` references
   - Where `useState` is not used to control a form, remove references to useState
