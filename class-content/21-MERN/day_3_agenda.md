# Week 21 Day 3 Agenda

1. `git pull`
1. Hi Allyssa!
1. Week 21 Activity 17: updating the Apollo client cache
1. Week 21 Activity 18: Debug the broken cache write
1. Review Activity 18
1. Week 21 Activity 19: Introduction to React Router
1. Week 21 Activity 20: Add a React Router to the Thoughts app
1. Review Activity 20
1. Week 21 Activity 21: Intro to JWT signing on the server
1. Week 21 Activity 22: Analyze the code
1. Review Activity 22
1. Week 21 Activity 23: JWT decoding on the client (client `context`)
1. Week 21 Activity 24: Debug the code
1. Review Activity 24
