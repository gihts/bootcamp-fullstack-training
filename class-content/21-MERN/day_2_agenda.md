# Week 21 Day 2 Agenda

1. `git pull`
1. Review GraphQL and Apollo Server
1. Week 21 Activity 9: Writing to Apollo Server with Mutations
1. Week 21 Activity 10: Debug the server mutation
1. Review Activity 10
1. Week 21 Activity 11: MERN Setup - client and server together
1. Week 21 Activity 12: Add comments to the README
1. Review Activity 12
1. Week 21 Activity 13: querying with the `useQuery` hook
1. Week 21 Activity 14: querying for thoughts
1. Review Activity 14
1. Week 21 Activity 15: mutating with the `useMutation` hook
1. Week 21 Activity 16: add thoughts
1. Review Activity 16
1. Week 21 Activity 17: updating the Appllo client cache
1. Week 21 Activity 18: Debug the broken cache write
1. Review Activity 18
1. Week 21 Activity 19: Introduction to React Router
1. Week 21 Activity 20: Add a React Router to the Thoughts app
1. Review Activity 20
