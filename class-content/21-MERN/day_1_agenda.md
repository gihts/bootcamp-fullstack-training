# Week 21 Day 1 Agenda

1. `git pull`
1. Everyone: Activity 27: Use `gh-pages` to deploy a React app to Github pages
1. MERN Stack!
1. Week 21 Activity 1: What are GraphQL and Apollo Server?
1. Week 21 Activity 2: Analyze the code...explore it
1. Review Activity 2
1. Week 21 Activity 3: Querying the Apollo Server
1. Week 21 Activity 4: Write some queries in GraphQL Playground
1. Review Activity 4
1. Week 21 Activity 5: Defining a query schema with `typedefs` and `resolvers`
1. Week 21 Activity 6: Complete the `resolver`
1. Review Activity 6
1. Week 21 Activity 7: Querying Applo Server using arguments
1. Week 21 Activity 8: query the server for a single class object
1. Review Activity 8
