# Agenda

1. Introduce Ming and Frantz!
1. `git pull`
1. Week 7: Intro to Projects
1. Slides - Projects
1. What is Agile Software Development?
1. What is an MVP?
1. What is a user story?
1. What are acceptance criteria?
1. What is an issue?
1. What is a kanban board?
1. What is daily standup?
1. Project 1 Requirements
1. Brainstorming
