# Agenda

1. `git pull`
1. Week 9 Activity 17: npm (node package manager)
1. Week 9 Activity 18: npm init your own project
1. Review Week 9 Activity 18
1. Week 9 Activity 19: inquirer.js - a command line interface helper
1. Week 9 Activity 20: build a cli
1. Review Week 9 Activity 20
1. Week 9 Activity 23: three dots - rest and spread
1. Week 9 Activity 24: analyze the code
1. Review Week 9 Activity 24
1. Week 9 Activity 25: object destructuring
1. Week 9 Activity 26: use object destructuring
1. Review Week 9 Activity 26
1. Week 9 Activity 27: Git Fork
1. Week 9 Activity 28: Mini Project - HTML file generator
1. Homework 9: A README Generator
