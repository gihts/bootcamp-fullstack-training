const inquirer = require("inquirer");
const fs = require("fs");

inquirer
  .prompt([
    {
      type: "input",
      name: "name",
      message: "What is your name?",
    },
    {
      type: "input",
      name: "location",
      message: "What is your location?",
    },
    {
      type: "input",
      name: "bio",
      message: "What is your bio?",
    },
    {
      type: "input",
      name: "food",
      message: "What is your favorite food?",
    },
    {
      type: "input",
      name: "github",
      message: "What is your GitHub?",
    },
    {
      type: "input",
      name: "linkedin",
      message: "What is your LinkedIn?",
    },
  ])
  .then((response) => {
    console.log(response);
    const html = generateHTML(response);
    const filename = `${response.name.toLowerCase().split(" ").join("")}.html`;
    fs.writeFile(filename, html, (err) =>
      err ? console.log(err) : console.log("Success!")
    );
  });

const generateHTML = function (answers) {
  const html = `<!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="ie=edge" />
      <link
        rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      />
      <title>Document</title>
    </head>
    <body>
      <div class="jumbotron jumbotron-fluid">
        <div class="container">
          <h1 class="display-4">Hi! My name is ${answers.name}</h1>
          <p class="lead">I am from ${answers.location}.</p>
          <h3>
            Example heading <span class="badge badge-secondary">Contact Me</span>
          </h3>
          <ul class="list-group">
            <li class="list-group-item">
              My GitHub username is ${answers.github}
            </li>
            <li class="list-group-item">LinkedIn: ${answers.linkedin}</li>
          </ul>
        </div>
      </div>
    </body>
  </html>
  `;

  return html;
};
