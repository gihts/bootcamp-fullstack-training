// Create an object
const myObj = {
  p: 1,
  e: 2,
  n: 3,
};

console.log("myObj", myObj);

// Make a "shallow" copy. This only makes a copy of the object's memory address (aka its reference)
// not the data held in memory
const thisIsAShallowCopy = myObj;
console.log("thisIsAShallowCopy", thisIsAShallowCopy);

// make a change to the shallow copy...
thisIsAShallowCopy.p = "hello";
console.log("thisIsAShallowCopy again", thisIsAShallowCopy);

// ... and it appears in the original
console.log("myObj Again...it has been modified!", myObj);

// Make a deep copy of the original. This creates an entirely new object from the original: memory,
// and data
const thisIsADeepCopy = { ...myObj };
console.log("thisIsADeepCopy", thisIsADeepCopy);

// Make a change to the deep copy...
thisIsADeepCopy.p = "I have changed";
console.log("thisIsADeepCopy again", thisIsADeepCopy);

// ...and the original has not been changed.
console.log("myObj Again...it has not been modified!", myObj);
