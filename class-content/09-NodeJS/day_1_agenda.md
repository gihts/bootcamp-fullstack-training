# Agenda

1. Project surveys
1. `git pull`
1. Slides: what is Node.js?
1. Demo mini project: HTML Generator
1. Week 9 Activity 1: Intro to node.js
1. Week 9 Activity 2: Run your first node program
1. Review Week 9 Activity 2
1. Introduction to ES6
1. Week 9 Activity 3: Arrow functions
1. Week 9 Activity 4: Debug the node program
1. Review Week 9 Activity 4
1. Week 9 Activity 5: `let` and `const` are a better `var`
1. Week 9 Activity 6: Debug the node program
1. Review Week 9 Activity 6
1. Week 9 Activity 7: Functional loops
