# Agenda

1. `git pull`
1. Self Check: ES6 and Node
1. Week 9 Activity 8: Comment the code
1. Review Week 9 Activity 8
1. Week 9 Activity 9: Template literals
1. Week 9 Activity 10: Build a song name with template literals
1. Review Week 9 Activity 10
1. Week 9 Activity 11: process.argv taking command line arguments
1. Week 9 Activity 12: command line app (compare parameters)
1. Review Week 9 Activity 12
1. Week 9 Activity 13: read from /write to a file from command line
1. Week 9 Activity 14: analyze the code
1. Review Week 9 Activity 14
1. Week 9 Activity 15: module.exports and require()
1. Week 9 Activity 16: Require the moth module
1. Review Week 9 Activity 16
