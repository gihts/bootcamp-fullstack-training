# Agenda

1. `git pull`
1. Week 6 Activity 19: `document.location`
1. Week 6 Activity 20: use `document.location` to redirect to a 404 page
1. Review Week 6 Activity 20
1. Week 6 Activity 21: Intro Review 1
1. Week 6 Activity 22: Review Exercise 1
1. Review Week 6 Activity 22
1. Week 6 Activity 23: Intro Review 2
1. Week 6 Activity 24: Review Exercise 2
1. Review Week 6 Activity 24
1. Week 6 Activity 25: Create and protect a Github project
1. Week 6 Activity 26: How to do Gitflow: `develop` and feature branches
1. Week 6 Activity 27: Mini-Project - Library of Congress
1. Homework 6: Weather Dashboard
