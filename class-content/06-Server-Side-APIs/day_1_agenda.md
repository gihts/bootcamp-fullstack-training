# Agenda

1. Introduce Ross!
1. `git pull`
1. Week 5 Activity 28: Project Tracker
1. Review Week 5 Activity 28
1. Slides - Server Side API's
1. Week 6 Activity 1: Intro to `curl` - make a request from the command line
1. Week 6 Activity 2: Debug the `curl` request
1. Review Week 6 Activity 2
1. Week 6 Activity 3: Intro to `fetch` - make a request from your code
1. Week 6 Activity 4: Fetch a list of your repos from github
1. Review Week 6 Activity 4
1. Week 6 Activity 5: More ways to fetch - `XMLHttpRequest` and `$.ajax`
1. Week 6 Activity 6: Add comments to describe `$.ajax`, `fetch` and `XMLHttpRequest`
1. Review Week 6 Activity 6
