# Agenda

1. Introduce Ross!
1. `git pull`
1. Week 6 Activity 7: parsing `response.json()`
1. Week 6 Activity 8: fetch and log issues from Twitter's Chill repo
1. Review Week 6 Activity 8
1. Week 6 Activity 9: write API response results to the DOM
1. Week 6 Activity 10: fetch and write 5 github users to the DOM
1. Review Week 6 Activity 10
1. Week 6 Activity 11: Checking Network activity in chrome dev tools
1. Week 6 Activity 12: Debug network error
1. Review Week 6 Activity 12
1. Week 6 Activity 13: Checking and handling `response.status`
1. Week 6 Activity 14: Handle a `response.status` of `404`
1. Review Week 6 Activity 14
1. Week 6 Activity 15: What are `GET` parameters?
1. Week 6 Activity 16: Explain the paramters
1. Review Week 6 Activity 16
1. Week 6 Activity 17: What are `fetch` options?
1. Week 6 Activity 18: Set the `cache` option
1. Review Week 6 Activity 18
