# Agenda

1. `git pull`
1. Homework - submit early and often
1. Week 3 Activity 28: Rock Paper Scissors
1. Review Week 3 Activity 28
1. Week 4: Intro to Web API's
1. Week 4 Activity 1: what is the `Window` Object?
1. Week 4 Activity 2: comment the code
1. Review Week 4 Activity 2
1. Week 4 Activity 3: how to traverse the DOM
1. Week 4 Activity 4: change style object properties
1. Review Week 4 Activity 4
1. Week 4 Activity 5: how to set element attributes
1. Week 4 Activity 6: change style attribute
1. Review Week 4 Activity 6
1. Week 4 Activity 7: create and append elements
