# Agenda

1. `git pull`
1. Week 4 Activity 17: Event propagation
1. Week 4 Activity 18: Comment the code
1. Review Week 4 Activity 18
1. Week 4 Activity 19: What are data attributes?
1. Week 4 Activity 20: Card "flipping"
1. Review Week 4 Activity 20
1. Week 4 Activity 21: What is localstorage?
1. Week 4 Activity 22: store the last registered user and email
1. Review Week 4 Activity 22
1. Week 4 Activity 23: How do I store an object?
1. Week 4 Activity 24: Store a user as an object
1. Review Week 4 Activity 24
1. Week 4 Activity 28: Mini Project - Word Guess game
