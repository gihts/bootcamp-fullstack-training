# Agenda

1. `git pull`
1. Homework - submit early and often
1. Week 4 Activity 7: create and append elements
1. Week 4 Activity 8: append and style some elements
1. Review Week 4 Activity 8
1. Week 4 Activity 9: Intro to timers - setInterval and clearInterval
1. Week 4 Activity 10: practice with timers
1. Review Week 4 Activity 10
1. Week 4 Activity 11: What is an Event? What is an Event Listener?
1. Week 4 Activity 12: Handle button clicks
1. Review Week 4 Activity 12
1. Week 4 Activity 13: Events default behavior (submitting forms)
1. Week 4 Activity 14: Prevent the form submission's default behavior
1. Review Week 4 Activity 14
1. Intro Homework 4: Code Quiz
1. Week 4 Activity 15: How to capture keyboard events
1. Week 4 Activity 16: Capture the KEYDOWN event
1. Review Week 4 Activity 16
1. Week 4 Activity 17: Event propagation
