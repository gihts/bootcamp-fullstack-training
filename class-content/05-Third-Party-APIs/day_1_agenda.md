# Agenda

1. `git pull`
1. Slides - Event loop
1. Week 4 Activity 28: word guess - 30 minutes
1. Review Week 3 Activity 28
1. Slides - Third Party API's
1. Week 5 Activity 1: What is jQuery?
1. Week 5 Activity 2: manipulate the DOM with jQuery
1. Week 5 Activity 2
1. Week 5 Activity 3: Handling click events in jQuery
1. Week 5 Activity 4: debug the password generator
1. Week 5 Activity 4
1. Week 5 Activity 5: using jQuery to get user input from forms
1. Week 5 Activity 6: jQuery shopping list
1. Week 5 Activity 6
