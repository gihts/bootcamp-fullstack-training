# Pre-Read: Week 5

## Day 1

- [WHAT IS A JAVASCRIPT LIBRARY?](https://generalassemb.ly/blog/what-is-a-javascript-library/#:~:text=Generally%20speaking%2C%20JavaScript%20libraries%20are,on%20an%20as%2Dneeded%20basis.)
- [Tech 101: What Is JQuery, And What Is It Used For?](https://skillcrush.com/blog/what-is-jquery-used-for/)
- [Should you use or learn jQuery in 2020?](https://flaviocopes.com/jquery/)
- [jQuery](https://api.jquery.com/)
- [DOM traversal and manipulation](https://api.jquery.com/category/traversing/)
- [Event handling](https://api.jquery.com/category/events/)

## Day 2

- [A Beginner’s Guide to CSS Front End Frameworks](https://blog.zipboard.co/a-beginners-guide-to-css-front-end-frameworks-8045a499456b)
- [What is Bootstrap: A Beginner's Guide](https://careerfoundry.com/en/blog/web-development/what-is-bootstrap-a-beginners-guide/)
- [Build fast, responsive sites with Bootstrap](https://getbootstrap.com/)

## Day 3

- [Google Fonts](https://fonts.google.com/)
- [jQueryUI](https://jqueryui.com/)
- [MomentJS](https://momentjs.com/)
- [You don't (may not) need Moment.js](https://github.com/you-dont-need/You-Dont-Need-Momentjs)
