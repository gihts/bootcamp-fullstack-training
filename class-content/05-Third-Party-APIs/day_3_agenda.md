# Agenda

1. `git pull`
1. Self-Check: Bootstrap
1. Week 5 Activity 15: bootstrap utility classes
1. Week 5 Activity 16: change component colors with utility classes
1. Review Week 5 Activity 16
1. Week 5 Activity 17: extending bootstrap - adding your own styles
1. Week 5 Activity 18: change component colors with your own style sheet
1. Review Week 5 Activity 18
1. Week 5 Activity 19: using 3rd party fonts - Google Fonts
1. Week 5 Activity 20: add some Google fonts
1. Review Week 5 Activity 20
1. Notes about jQuery UI
1. Week 5 Activity 25: date math and formatting with Moment
1. Week 5 Activity 26: answer some date questions
1. Review Week 5 Activity 26
1. Week 5 Activity 27: Git Revert
1. Week 5 Activity 28: Mini Project - Project List
1. Homework 5: A Daytimer
