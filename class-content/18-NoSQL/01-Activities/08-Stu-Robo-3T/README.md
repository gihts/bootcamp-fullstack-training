# Robot 3T

- In this activity, you will practice using Robo 3T.

## Instructions

- Drop your classroom collection and create a new one.

  - `right-click on Collections -> Create Collection...`

- In a new classroom collection, re-enter your `name`, `os`, and `hobby` info array.

  - This should be entered using the `right-click -> Insert Document` method.

- Next, Slack out your `name`, `os` and `hobbies` into the classroom chat.

- As students enter their info into slack, insert it into your database.

- By the end of the exercise, you should have every student's information in your classroom collection.
