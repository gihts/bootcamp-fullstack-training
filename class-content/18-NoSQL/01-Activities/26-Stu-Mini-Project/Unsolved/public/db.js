let db;
// 1. create a new db request for a "BudgetDB" database.

request.onupgradeneeded = function (event) {
  // 2. create object store called "BudgetStore" and set autoIncrement to true
};

request.onsuccess = function (event) {
  db = event.target.result;

  if (navigator.onLine) {
    checkDatabase();
  }
};

request.onerror = function (event) {
  // 3. log error here
};

function saveRecord(record) {
  // 4. create a transaction on the BudgetDB db with readwrite access
  // 5. access your BudgetStore object store
  // 6. add record to your store with add method.
}

function checkDatabase() {
  // 7. open a transaction on your BudgetDB db
  // 8. access your BudgetStore object store
  // 9. create a variable called getAll that gets all records from store

  getAll.onsuccess = function () {
    if (getAll.result.length > 0) {
      fetch("/api/transaction/bulk", {
        method: "POST",
        body: JSON.stringify(getAll.result),
        headers: {
          Accept: "application/json, text/plain, */*",
          "Content-Type": "application/json",
        },
      })
        .then((response) => response.json())
        .then(() => {
          // 10. if successful, open a transaction on your BudgetDB db
          // 11. access your BudgetStore object store
          // 12. clear all items in your store
        });
    }
  };
}

// listen for app coming back online
window.addEventListener("online", checkDatabase);
