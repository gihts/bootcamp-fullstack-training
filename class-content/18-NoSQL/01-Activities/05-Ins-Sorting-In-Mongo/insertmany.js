db.animals.insertMany([
  { name: "Panda", numLegs: 4, class: "mammal", weight: 254 },
  { name: "Human", numLegs: 2, class: "mammal", weight: 190 },
  { name: "Spider", numLegs: 8, class: "arachnid", weight: 0.01 },
  { name: "Crow", numLegs: 2, class: "bird", weight: 3 },
]);

db.animals.insert({
  name: "Panda",
  numLegs: 4,
  class: "mammal",
  weight: 254,
});
db.animals.insert({
  name: "Dog",
  numLegs: 4,
  class: "mammal",
  weight: 60,
});
db.animals.insert({
  name: "Ostrich",
  numLegs: 2,
  class: "aves",
  weight: 230,
});
db.animals.insert({
  name: "Kangaroo",
  numLegs: 2,
  class: "marsupial",
  weight: 200,
});
db.animals.insert({
  name: "Chameleon",
  numLegs: 4,
  class: "reptile",
  weight: 5,
});
