# Week 18 Day 2 agenda

1. `git pull`
1. Intro to Career Services
1. Week 18 Activity 9: Review routes with Mongo
1. Review Week 18 Activity 9
1. Week 18 Activity 10: Introduction to mongoose
1. Week 18 Activity 11: Create a User model
1. Review Week 18 Activity 11
1. Week 18 Activity 12: Custom methods in Mongoose
1. Week 18 Activity 13: Add some custom methods to the User model
1. Review Week 18 Activity 13
1. Week 18 Activity 14: Populate relations
1. Week 18 Activity 15: Populate Users with Notes
1. Review Week 18 Activity 15
