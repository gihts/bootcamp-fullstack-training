# Week 18 Day 3 agenda

1. check-in to BCS!
1. `git pull`
1. Week 18 Activity 16: Introduction to IndexedDB
1. Week 18 Activity 17: Create an IndexedDB connection
1. Review Week 18 Activity 17
1. Week 18 Activity 18: What is an IndexedDB object store?
1. Week 18 Activity 19: Create an object store
1. Review Week 18 Activity 19
1. Week 18 Activity 20: What is an IndexedDB index?
1. Week 18 Activity 21: Add some indexes
1. Review Week 18 Activity 21
1. Week 18 Activity 22: How to add and get data with `transactions`
1. Week 18 Activity 23: Add some todo items
1. Review Week 18 Activity 23
1. Week 18 Activity 24: Traverse a record set using a cursor
1. Week 18 Activity 25: Update the todo item's status
1. Review Week 18 Activity 25
1. Week 18 Activity 26: Mini Project - Budget Tracker
1. Review Week 18 Activity 26
1. Homework 18: Exercise Tracker
