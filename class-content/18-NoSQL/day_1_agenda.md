# Week 18 Day 1 agenda

1. `git pull`
1. [Make sure you have installed MongoDB](https://coding-boot-camp.github.io/full-stack/mongodb/how-to-install-mongodb)
1. [Install Robo3T](https://robomongo.org/download)
1. Slides: Intro to MongoDB
1. CRUD in MongoDB
1. Week 18 Activity 1: Create a database, insert some documents
1. Week 18 Activity 2: Create a `classroom` database
1. Review Week 18 Activity 2
1. Week 18 Activity 3: Update, Delete documents
1. Week 18 Activity 4: Update the `classroom` data
1. Review Week 18 Activity 4
1. Week 18 Activity 5: Sorting, `mongojs`
1. Week 18 Activity 6: Sort with `mongojs`
1. Review Week 18 Activity 6
1. Week 18 Activity 7: CRUD with `mongojs`
1. Review Week 18 Activity 7
1. Week 18 Activity 8: Using Robo3T
1. Review Week 18 Activity 8
