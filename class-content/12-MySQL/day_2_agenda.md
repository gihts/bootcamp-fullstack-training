# Agenda

1. Homework Jumpstart: Watch class video pre and post office hours for great hints
1. Ben is still sick. Hi Andrew!
1. `git pull`
1. Review: parts of MySQL
1. Week 12 Activity 6: Node with MySQL - create a connection
1. Week 12 Activity 6: Students run the code! (10 mins)
1. Week 12 Activity 7: Node with MySQL - read from the DB
1. Week 12 Activity 8: Create a playlist (15 mins)
1. Review Week 12 Activity 8
1. Week 12 Activity 9: what's CRUD?
1. Week 12 Activity 9: Students CRUD! (20 mins)
1. Week 12 Activity 10: Great Bay Mini Project
1. Post Class Office Hours: Homework 10 check-in
