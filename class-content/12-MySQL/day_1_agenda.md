# Agenda

1. Pre Class Office Hours - Homework Jumpstart: Homework 10 OOP Team Generator
1. Ben is sick. Hi Frantz!
1. `git pull`
1. Slides: What is MySQL?
1. MySQL and MySQL Workbench Installation
1. Instructor: Create a localhost connection to the database
1. Students: Create a localhost connection
1. Instructor: Connection vs. Database
1. Week 12 Activity 1: Create a Database and a Table
1. Week 12 Activity 2: Set up a favorites DB
1. Review Week 12 Activity 2
1. Instructor: `SELECT`, `INSERT`, `UPDATE` the `people` table
1. Students: Add favorites to the favorites database
1. Week 12 Activity 3: what are primary keys and unique identifiers
1. Week 12 Activity 4: Make the programming_db
1. Review Week 12 Activity 4
1. Week 12 Activity 4: linking tables with a `JOIN`
