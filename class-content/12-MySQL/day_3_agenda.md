# Agenda

1. What is Fastly?
1. Week 12 Activity 10: Great Bay Mini Project
1. Review Week 12 Activity 10
1. Week 12 Activity 12: Data analysis - Top Songs Schema
1. Review Week 12 Activity 12
1. Week 12 Activity 12: How to import CSV data
1. Week 12 Activity 13: Write some queries
1. Week 12 Activity 14: Two Tables - how do these data sets relate?
1. Homework 12: Employee Tracker
