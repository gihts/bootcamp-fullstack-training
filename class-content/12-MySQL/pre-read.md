# Week 12 Pre Read - MySQL

## Day 1

- [MySQL Installer guides for Mac ](../04-Important/mysql-mac-guide.md)
- [MySQL Installer guides for PC ](../04-Important/mysql-windows-guide.md)
- [What Is MySQL](https://www.mysqltutorial.org/what-is-mysql/)
- [Understanding Relational Databases](https://www.digitalocean.com/community/tutorials/understanding-relational-databases)
- [An Introduction to Queries in MySQL](https://www.digitalocean.com/community/tutorials/introduction-to-queries-mysql)

## Day 2

- [mysql npm package](https://www.npmjs.com/package/mysql)
- [Connecting to the MySQL Database Server from Node.js](https://www.mysqltutorial.org/mysql-nodejs/connect/)

## Day 3

- [SQL Bolt](https://sqlbolt.com/)
