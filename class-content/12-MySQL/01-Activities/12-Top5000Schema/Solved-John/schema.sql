DROP DATABASE IF EXISTS topsongs_db;

CREATE DATABASE topsongs_db;

use topsongs_db;

CREATE TABLE songs (
  id INT AUTO_INCREMENT NOT NULL,
  position INT NOT NULL,
  artist VARCHAR(256),
  song VARCHAR(256),
  year INT,
  popscore_total DECIMAL(10, 4) NOT NULL,
  popscore_us DECIMAL(10, 4) NOT NULL,
  popscore_uk DECIMAL(10, 4) NOT NULL,
  popscore_eu DECIMAL(10, 4) NOT NULL,
  popscore_row DECIMAL(10, 4) NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO songs (position, artist, song, year, popscore_total, popscore_us, popscore_uk, popscore_eu, popscore_row) VALUES (1,Bing Crosby,White Christmas,1942,39.903,23.929,5.7,2.185,0.54);

INSERT INTO songs (position, artist, song, year, popscore_total, popscore_us, popscore_uk, popscore_eu, popscore_row) VALUES (2,Bill Haley & his Comets,Rock Around the Clock,1955,36.503,19.961,7.458,5.663,0.76);

INSERT INTO songs (position, artist, song, year, popscore_total, popscore_us, popscore_uk, popscore_eu, popscore_row) VALUES (3,Celine Dion,My Heart Will Go On,1998,35.405,12.636,8.944,23.701,3.61);