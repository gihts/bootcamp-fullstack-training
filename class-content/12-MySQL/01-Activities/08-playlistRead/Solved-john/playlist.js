const mysql = require("mysql");

const connection = mysql.createConnection({
  host: "localhost",
  port: 3306,
  user: "root",
  password: "password",
  database: "playlistdb",
});

function querySongs(cb) {
  connection.query("SELECT * from songs", (err, result) => {
    if (err) throw err;

    console.log(result);
    cb();
  });
}

function queryPopSongs(cb) {
  connection.query("SELECT * from songs WHERE genre='pop'", (err, result) => {
    if (err) throw err;

    console.log(result);
    cb();
  });
}

connection.connect((err) => {
  if (err) throw err;
  let tasks = 0;

  querySongs(checkTasks);
  queryPopSongs(checkTasks);

  function checkTasks() {
    tasks++;
    if (tasks == 2) {
      connection.end();
      return;
    }

    console.log(tasks);
  }
});
