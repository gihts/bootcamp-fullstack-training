DROP DATABASE IF EXISTS  playlistdb;

CREATE DATABASE playlistdb;

use playlistdb;

CREATE TABLE songs (
  id INTEGER AUTO_INCREMENT NOT NULL,
  title VARCHAR(256) NOT NULL,
  artist VARCHAR(128) NOT NULL,
  genre VARCHAR(128) NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO songs (title, artist, genre) VALUES ("Hey Jude", "Beatles", "pop");
INSERT INTO songs (title, artist, genre) VALUES ("Thriller", "Michael Jackson", "pop");
INSERT INTO songs (title, artist, genre) VALUES ("So what", "Miles Davis", "Jazz");