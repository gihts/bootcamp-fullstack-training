///////////////////////////////////////////////////////////////////////////
// 3 methods of handling asynchronous javascript
///////////////////////////////////////////////////////////////////////////

const fs = require("fs");

///////////////////////////////////////////////////////////////////////////////////
// 1. The callback
// We see this in a lot of standard Node libraries like fs

console.log("Let's read a file...");
fs.readFile("log.txt", "utf8", function (err, data) {
  if (err) {
    console.log("There was an error", err);
    process.exit();
  }

  console.log("Read a file. Got some data!\n-------------------\n", data);
});

// here is a another common callback implementation

// guessMyName is an asynchronous function. The setTimeout will not finish until way after
// guessMyName completes its execution
function guessMyName(myName, cb, errCb) {
  setTimeout(function () {
    if (myName === "john") {
      cb("you got it!");
    } else {
      errCb("nope! that's not it");
    }
  }, 5000);

  console.log("After starting timer in guessMyName");
}

guessMyName(
  "jimmy",
  function (result) {
    console.log("This was a name guess", result);
  },
  function (err) {
    console.log("There was an error guessing my name", err);
  }
);

//////////////////////////////////////////////////////////////////////////////////////////
// 2. the Promise - this is the preferred way. Added to javascript in ES2015
// Async function calls often depend on the result of another async function call. To do
// this, callbacks end up becoming nested (e.g., a callback that takes a callback that takes a
// callback...and so on). This is called callback hell.
//
// Promises fix this by chaining

const axios = require("axios");

console.log("Calling weather data with axios!");

// axios is a npm package for making remote api calls
//
// axios.get returns a Promise...
axios
  .get(
    "https://api.openweathermap.org/data/2.5/weather?q=London&appid=78c022ae7b87430bbaabb56f3fd651a0"
  )
  // that is handled by this then
  .then(function (response) {
    console.log("Got the first weather data response\n", response.data);

    // this returns a Promise
    return axios.get(
      `https://api.openweathermap.org/data/2.5/onecall?lat=${response.data.coord.lat}&lon=${response.data.coord.lon}&appid=78c022ae7b87430bbaabb56f3fd651a0`
    );
  })
  // which is handled by this "then"
  .then(function (secondResponse) {
    console.log(
      "Got the second weather data response\n",
      secondResponse.data.current
    );
  })
  .catch(function (err) {
    console.log("Error: " + err);
  });

/////////////////////////////////////////////////////////////////////////////////////////////////////
// 3. async/await which is really just syntacic sugar for a promise. Added to Javascript in ES2017
// This doesn't work for callbacks, just for functions that return a Promise.

// a function that uses async calls needs to have "async" before its definition
(async function () {
  // in order to catch errors, we wrap the async calls in a try/catch block
  try {
    // the actual asynchronous call is prefaced by "await". This makes the code appear to be more synchronous
    const response1 = await axios.get(
      "https://api.openweathermap.org/data/2.5/weather?q=London&appid=78c022ae7b87430bbaabb56f3fd651a0"
    );

    // This will not execute until line 90 is finished
    const response2 = await axios.get(
      `https://api.openweathermap.org/data/2.5/onecall?lat=${response1.data.coord.lat}&lon=${response1.data.coord.lon}&appid=78c022ae7b87430bbaabb56f3fd651a0`
    );

    console.log(response2.data.current);
  } catch (err) {
    console.log("Error: " + err);
  }
})();

// This is an IIFE (Immediately Invoked Function Expression)
(function () {
  console.log("Hi there");
})();
