# Agenda

1. Jen was going to drop by
1. `git pull`
1. Slides: Object Oriented Programming
1. Demo mini project: Word Guessing Game
1. Week 10 Activity 1: What is a constructor function?
1. Week 10 Activity 2: create objects with a constructor
1. Review Week 10 Activity 2
1. Week 10 Activity 3: What is the `prototype`?
1. Week 10 Activity 4: analyze the code
1. Review Week 10 Activity 4
1. Async Javascript: 3 ways to handle async calls
1. Week 10 Activity 7: Making Promises
1. Week 10 Activity 8: Refactor a callback to be a promise
1. Review Week 10 Activity 8
