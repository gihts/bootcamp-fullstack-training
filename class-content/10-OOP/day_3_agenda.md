# Agenda

1. `git pull`
1. Week 10 Activity 21: Intro to Classes
1. Week 10 Activity 22: Create a Character class
1. Review Week 10 Activity 22
1. Week 10 Activity 23: `extends` - How to subclass
1. Week 10 Activity 24: Create some subclasses
1. Review Week 10 Activity 24
1. Week 10 Activity 25: Multiple Classes - the restaurant example
1. Week 10 Activity 26: Debug the Store using tests
1. Review Week 10 Activity 26
1. Week 10 Activity 28: Mini-Project - Letter guessing game, write to the tests
1. Homework 10: Software Dev Team Generator
