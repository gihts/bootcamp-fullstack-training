# Agenda

1. Jen should be dropping by
1. `git pull`
1. Week 10 Activity 11: Introduction to TDD with Jest
1. Week 10 Activity 12: write some tests
1. Review Week 10 Activity 12
1. Week 10 Activity 13: TDD Lifecycle - Red, Green, Refactor
1. Week 10 Activity 14: Write code to pass the test
1. Review Week 10 Activity 14
1. Week 10 Activity 15: Arrange, Act, Assert - organizing tests
1. Week 10 Activity 16: Analyze the code
1. Review Week 10 Activity 16
1. Week 10 Activity 17: Mocks: emulating external services
