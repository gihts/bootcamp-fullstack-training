# Week 19 Pre Read

## Day 1

- [Web Performance](<https://en.wikipedia.org/wiki/Web_performance#:~:text=Web%20performance%20optimization%20(WPO)%2C,and%20those%20on%20mobile%20devices.>)
- [Why Performance Matters](https://developers.google.com/web/fundamentals/performance/why-performance-matters/)
- [Measure Web Performance With The RAIL Model](https://developers.google.com/web/fundamentals/performance/rail)
- [Audit the Performance of Your Web Application](https://developers.google.com/web/fundamentals/performance/audit/)
- [Lighthouse](https://developers.google.com/web/tools/lighthouse)
- [JSCompress](https://jscompress.com/)
- [Compression in HTTP](https://developer.mozilla.org/en-US/docs/Web/HTTP/Compression)

## Day 2

- [What is a progressive web app in 60 seconds!](https://www.youtube.com/watch?v=Z8MjdQGyjfA)
- [Introduction to progressive web apps](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Introduction)
- [Your First Progressive Web App](https://developers.google.com/web/fundamentals/codelabs/your-first-pwapp/)
- [How Service Worker Cache Works - Does It Make Your Site Faster?](https://love2dev.com/blog/service-worker-cache/)
- [Workbox - Service Worker Libraries](https://developers.google.com/web/tools/workbox)

## Day 3

- [A Beginner’s Guide to Webpack](https://www.sitepoint.com/webpack-beginner-guide/)
- [webpack-bundle-analyzer](https://www.npmjs.com/package/webpack-bundle-analyzer)
- [Babel is a JavaScript compiler](https://babeljs.io/)
- [JavaScript Transpilers: What They Are & Why We Need Them](https://scotch.io/tutorials/javascript-transpilers-what-they-are-why-we-need-them)
- [JavaScript — WTF is ES6, ES8, ES 2017, ECMAScript… ?](https://codeburst.io/javascript-wtf-is-es6-es8-es-2017-ecmascript-dca859e4821c#:~:text=ES6%20%2F%20ES2015&text=You%20see%2C%20ES6%20and%20ES2015,reflect%20the%20year%20of%20release.)
