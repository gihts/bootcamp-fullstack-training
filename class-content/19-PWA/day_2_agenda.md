# Week 19 Day 2 Agenda

1. `git pull`
1. How did it go this weekend?
1. Job search strategy: networking and start small
1. What is a progressive web application?
1. Week 19 Activity 7: Add a PWA to your phone
1. Review Activity 7
1. Week 19 Activity 8: What is a Web Manifest?
1. Week 19 Activity 9: Create a Web Manifest
1. Review Activity 9
1. Week 19 Activity 10: What is a Service Worker?
1. Week 19 Activity 11: Register a service worker
1. Review Activity 11
1. Week 19 Activity 12: How to use the service worker for caching
1. Week 19 Activity 13: Make the images cache
1. Review Activity 13
1. Week 19 Activity 14: Convert Trip Planner into PWA
1. Review Activity 14
