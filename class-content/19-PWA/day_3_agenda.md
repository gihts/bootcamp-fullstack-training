# Week 19 Day 3 Agenda

1. `git pull`
1. Automating optimization
1. Week 19 Activity 15: Intro to `webpack`
1. Week 19 Activity 16: Bundle js for Expenses Tracker
1. Review Activity 16
1. Week 19 Activity 17: What is a `webpack` plugin?
1. Week 19 Activity 18: use `webpack-pwa-manifest` to bundle Image Gallery PWA
1. Review Activity 18
1. Week 19 Activity 19: ES6 Modules and transpiling with Babel
1. Week 19 Activity 20: Refactor app.js and bundle with `webpack` and Babel
1. Review Activity 20
1. Week 19 Activity 21: Chunking our javascript for lazy loading
1. Week 19 Activity 22: Lazy load javascript modules in Image Gallery app
1. Review Activity 22
1. Week 19 Activity 23: Mini project - make news aggregator application a PWA
1. Week 19 Homework: Online/Offline Budget Tracker
