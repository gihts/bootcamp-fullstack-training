# Week 20 Day 3 Agenda

1. `git pull`
1. Week 20 Activity 19: The`useEffect` hook: what is a "side effect"?
1. Week 20 Activity 20: Use `useEffect` hook to set the `document.title`
1. Review Activity 20
1. What is conditional rendering?
1. Week 20 Activity 21: Styles in React - `.css` files and inline styles
1. Week 20 Activity 22: Adds om einline styles
1. Review Activity 22
1. Week 20 Activity 23: Conditional rendering with the ternary operator
1. Week 20 Activity 24: Analyze the code for a multipage app
1. Review Activity 24
1. Week 20 Activity 25: Testing React components with Jest
1. Week 20 Activity 26: add the `render` code to complete the test
1. Review Activity 26
