# Week 20 Day 2 Agenda

1. `git pull`
1. Review the front end event loop
1. Week 20 Activity 11: State in React: `useState`
1. Week 20 Activity 12: Use state to render a greeting to class members
1. Review Activity 12
1. Week 20 Activity 13: Update state to update the UI
1. Week 20 Activity 14: Analyze the code, write some comments
1. Review Activity 14
1. Week 20 Activity 15: Controlled component - form handling with React
1. Week 20 Activity 16: Add a password field to a form
1. Review Activity 16
1. Week 20 Activity 17: Class component; fetch data
1. Week 20 Activity 18: Make a controlled component, fetch OMDB data
1. Review Activity 18
