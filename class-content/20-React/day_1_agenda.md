# Week 20 Day 1 Agenda

1. `git pull`
1. Introduction to React
1. Week 20 Activity 1: what's `create-react-app`?
1. Week 20 Activity 2: use `create-react-app`
1. Review Activity 2
1. Week 20 Activity 3: Render a DOM element
1. Week 20 Activity 4: Create an `h1`
1. Review Activity 4
1. Week 20 Activity 5: JSX, sub components, passing props
1. Week 20 Activity 6: Modify the `List` component to render the list of groceries
1. Review Activity 6
1. Week 20 Activity 7: more on JSX
1. Week 20 Activity 8: insert some javascript expressions into the JSX
1. Review Activity 8
1. Week 20 Activity 9: more on `props`
1. Week 20 Activity 10: pass props to render a card
1. Review Activity 10
