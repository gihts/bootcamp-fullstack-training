import React, { useState } from "react";
import BucketListItemForm from "./BucketListItemForm";
import BucketListItem from "./BucketListItem";

function BucketList(props) {
  /**
   * A BucketListItem has:
   * id int
   * value string
   * eagerness enum("high", "medium", "low")
   */
  const [bucketListItemToEdit, setBucketListItemToEdit] = useState({
    id: null,
    value: "",
    eagerness: "",
  });

  console.log(props.bucketListItems);

  const submitUpdate = (item) => {
    // TODO: Write logic to update the `edit` value in state after a user updates an entry in the list
    props.editBucketListItem(bucketListItemToEdit.id, item);
    // TODO: Set the key:value pairs in the `edit` object back to empty strings
    setBucketListItemToEdit({
      id: null,
      value: "",
      eagerness: "",
    });
  };

  // If the user is attempting to edit an item, render the bucket form with the edit variable passed as a prop
  if (bucketListItemToEdit.id) {
    return (
      <BucketListItemForm
        bucketListItemToEdit={bucketListItemToEdit}
        onSubmit={submitUpdate}
      />
    );
  }

  return props.bucketListItems.map((item) => (
    <BucketListItem
      key={item.id}
      item={item}
      completeBucketListItem={props.completeBucketListItem}
      setBucketListItemToEdit={setBucketListItemToEdit}
      removeBucketListItem={props.removeBucketListItem}
    />
  ));
}

export default BucketList;
