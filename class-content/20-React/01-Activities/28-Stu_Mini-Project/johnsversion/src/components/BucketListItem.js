export default function BucketListItem({
  item,
  completeBucketListItem,
  setBucketListItemToEdit,
  removeBucketListItem,
}) {
  return (
    <div
      className={
        item.isComplete
          ? `bucket row complete ${item.eagerness}`
          : `bucket-row ${item.eagerness}`
      }
      key={item.id}
    >
      <div key={item.id} onClick={() => completeBucketListItem(item.id)}>
        {item.value}
      </div>
      <div className="icons">
        <p
          onClick={() => {
            setBucketListItemToEdit({
              id: item.id,
              value: item.value,
              eagerness: item.eagerness,
            });
          }}
        >
          {" "}
          ✏️
        </p>

        <p onClick={() => removeBucketListItem(item.id)}> 🗑️</p>
      </div>
    </div>
  );
}
