import React, { useState } from "react";
import BucketListItemForm from "./BucketListItemForm";
import BucketList from "./BucketList";

function AppContainer() {
  const [bucketListItems, setBucketListItems] = useState([]);

  // Function to add a bucket list item
  const addBucketListItem = (item) => {
    // TODO: Write logic to add the new bucket item to the bucket state variable
    if (!item.value) {
      return;
    }

    const newState = [item, ...bucketListItems];
    setBucketListItems(newState);
  };

  // Function to mark bucket list item as complete
  const completeBucketListItem = (id) => {
    // If the ID passed to this function matches the ID of the item that was clicked, mark it as complete
    let updatedBucket = bucketListItems.map((item) => {
      // TODO: Write logic that marks an item as complete or incomplete when invoked
      if (item.id === id) {
        item.isComplete = true;
      }
      return item;
    });

    setBucketListItems(updatedBucket);
  };

  // Function to remove bucket list item and update state
  const removeBucketListItem = (id) => {
    // TODO: Write logic that will return an array of items that don't contain the ID passed to this function
    const newState = bucketListItems.filter((item) => item.id !== id);
    // TODO: Update the bucket state variable
    setBucketListItems(newState);
  };

  // Function to edit the bucket list item
  const editBucketListItem = (itemId, newItem) => {
    console.log("edit bucket form", itemId);
    console.log("edit bucket form value", newItem);

    // Make sure that the value isn't empty
    if (!newItem.value) {
      return;
    }

    // We use the "prev" argument provided with the useState hook to map through our list of items
    // We then check to see if the item ID matches the id of the item that was clicked and if so, we set it to a new value
    setBucketListItems((prev) =>
      prev.map((item) => (item.id === itemId ? newItem : item))
    );
  };

  return (
    <div>
      <h1>What is on your bucket list?</h1>
      <BucketListItemForm onSubmit={addBucketListItem} />
      <BucketList
        bucketListItems={bucketListItems}
        completeBucketListItem={completeBucketListItem}
        removeBucketListItem={removeBucketListItem}
        editBucketListItem={editBucketListItem}
      ></BucketList>
    </div>
  );
}

export default AppContainer;
