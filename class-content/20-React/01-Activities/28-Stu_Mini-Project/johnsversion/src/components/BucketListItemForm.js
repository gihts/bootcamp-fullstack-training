import React, { useState } from "react";

function BucketListItemForm(props) {
  console.log(props);
  const [bucketListItemLabel, setBucketListItemLabel] = useState(
    props?.bucketListItemToEdit?.value ? props.bucketListItemToEdit.value : ""
  );
  let [bucketListItemEagerness, setBucketListItemEagerness] = useState(
    props?.bucketListItemToEdit?.eagerness
      ? props.bucketListItemToEdit.eagerness
      : ""
  );

  // TODO: Use this array in the return statement below
  const eagernessLevel = ["high", "medium", "low"];

  const handleSubmit = (e) => {
    e.preventDefault();

    console.log("here is the label", bucketListItemLabel);

    if (!bucketListItemEagerness) {
      bucketListItemEagerness = "low";
    }

    props.onSubmit({
      id: props?.bucketListItemToEdit?.id
        ? props.bucketListItemToEdit.id
        : Math.random(Math.floor() * 1000),
      value: bucketListItemLabel,
      eagerness: bucketListItemEagerness,
    });

    setBucketListItemLabel("");
    setBucketListItemEagerness("");
  };

  const handleChange = (e) => {
    setBucketListItemLabel(e.target.value);
  };

  // First we check to see if "edit" prop exists. If not, we render the normal form
  // If the prop "edit" exists, we know to render the update form instead
  return !props.bucketListItemToEdit ? (
    <div>
      <form className="bucket-form" onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Add to your bucket list"
          value={bucketListItemLabel}
          name="text"
          className="bucket-input"
          onChange={handleChange}
        ></input>
        <div className="dropdown">
          <button className={`dropbtn ${bucketListItemEagerness}`}>
            {bucketListItemEagerness || "Priority"}
          </button>
          <div className="dropdown-content">
            {/* TODO: Add an onClick event that will set the corresponding eagerness level from the `eagernessLevel` array */}
            <p
              onClick={() => {
                setBucketListItemEagerness(eagernessLevel[0]);
              }}
            >
              Must do
            </p>
            <p
              onClick={() => {
                setBucketListItemEagerness(eagernessLevel[1]);
              }}
            >
              Want to do
            </p>
            <p
              onClick={() => {
                setBucketListItemEagerness(eagernessLevel[2]);
              }}
            >
              Take it or leave it
            </p>
          </div>
        </div>
        <button className="bucket-button">Add bucket list item</button>
      </form>
    </div>
  ) : (
    <div>
      <h3>Update entry: {props.bucketListItemToEdit.value}</h3>
      <form className="bucket-form" onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder={props.bucketListItemToEdit.value}
          value={bucketListItemLabel}
          name="text"
          className="bucket-input"
          onChange={handleChange}
        ></input>
        <div className="dropdown">
          <button className={`dropbtn ${bucketListItemEagerness}`}>
            {bucketListItemEagerness || "Priority"}
          </button>
          <div className="dropdown-content">
            {/* TODO: Add an onClick event that will set the corresponding eagerness level from the `eagernessLevel` array */}
            <p
              onClick={() => {
                setBucketListItemEagerness(eagernessLevel[0]);
              }}
            >
              Must do
            </p>
            <p
              onClick={() => {
                setBucketListItemEagerness(eagernessLevel[1]);
              }}
            >
              Want to do
            </p>
            <p
              onClick={() => {
                setBucketListItemEagerness(eagernessLevel[2]);
              }}
            >
              Take it or leave it
            </p>
          </div>
        </div>
        <button className="bucket-button">Update</button>
      </form>
    </div>
  );
}

export default BucketListItemForm;
