import "./App.css";
import AppContainer from "./components/AppContainer";

function App() {
  return (
    <div className="bucket-app">
      <AppContainer />
    </div>
  );
}

export default App;
