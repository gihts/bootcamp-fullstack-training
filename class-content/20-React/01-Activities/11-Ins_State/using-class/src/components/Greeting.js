// First we import `useState` with React so that we can take advantage of the hook
import React, { Component } from "react";

class Greeting extends Component {
  // State is kept as a property on the component. It is an object with properties
  state = {
    greeting: "Welcome! React state is awesome!",
  };

  // Part of the Component lifecycle. Will execute once the component is loaded
  componentDidMount() {
    // everytime setState is run, the component re-renders
    this.setState({ greeting: "This is the new state" });
  }

  // A class component needs a render function
  render() {
    return (
      <div className="card text-center">
        <div className="card-header bg-primary text-white">
          Greeting from state:
        </div>
        <div className="card-body">
          <p className="card-text" style={{ fontSize: "50px" }}>
            {this.state.greeting}
          </p>
        </div>
      </div>
    );
  }
}

export default Greeting;
