import { ADD_CAR, REMOVE_CAR } from "./actions";
const randomNum = () => Math.floor(Math.random() * 20);

// A reducer controls the possible changes to state
// it recieves the old state and an action. An action is an object:
// {
//    type: String
//    payload: mixed
// }
//
// The action type is a command for the reducer to do
// The payload is optional but is usally data used to affect state change. It
// can be any data type. It depends on the need of the reducer.
//
export const reducer = (state, action) => {
  switch (action.type) {
    case ADD_CAR: {
      const newID = randomNum();
      const newCar = { ...action.payload, id: newID };

      return {
        ...state,
        cars: [...state.cars, newCar],
      };
    }
    case REMOVE_CAR: {
      return {
        ...state,
        cars: [...state.cars].filter((car) => car.id !== action.payload),
      };
    }
    default: {
      return state;
    }
  }
};
