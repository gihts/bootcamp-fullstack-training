import React, { useState, useContext } from "react";

// Create our theme context using React.CreateContext()
export const CarContext = React.createContext();

// Create a custom hook that allows easy access to our CarContext values
export const useCar = () => useContext(CarContext);

const randomNum = () => Math.floor(Math.random() * 20);

// Creating our car provider. Accepts an argument of "props", here we plucking off the "children" object.
export default function CarProvider({ children }) {
  const [cars, setCars] = useState([
    {
      id: randomNum(),
      make: "Honda",
      model: "Civic",
      year: "2008",
    },
    {
      id: randomNum(),
      make: "Tesla",
      model: "Y",
      year: "2021",
    },
  ]);

  /**
   * NOTE: the reducers are meant to replace the follwoing functions (see activity 09-Ins_useReducer)
   *
   * To see the reducers work for this activity, run the tests
   *
   */

  const addCar = (car) => {
    const newID = randomNum();
    const newCar = { ...car, id: newID };

    // state needs a new object or array. This will create a new array, populate it with the elements in the old
    // state array and merge in the new object
    setCars([...cars, newCar]);
  };

  const removeCar = (id) => {
    const updatedCarList = cars.filter((car) => car.id !== id);

    // state needs a new object or array. The array filter function creates a new array
    setCars(updatedCarList);
  };

  // The provider component will wrap all other components inside of it that need access to our global state
  return (
    // the cars state variable and the addCare and removeCar functions are getting provided to the child components
    <CarContext.Provider value={{ cars, addCar, removeCar }}>
      {children}
    </CarContext.Provider>
  );
}
