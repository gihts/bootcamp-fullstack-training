import React, { useState } from "react";

// a context has a provider and a consumer
export const UserContext = React.createContext();

// Creating our own class to wrap the provider provided by the Context
const UserProvider = (props) => {
  // we are using this provider to control this state variable
  const [currentUser, setCurrentUser] = useState({
    name: "John",
    role: "Admin",
    id: 142323,
  });

  return (
    <UserContext.Provider value={{ currentUser: currentUser }} {...props} />
  );
};

export default UserProvider;
