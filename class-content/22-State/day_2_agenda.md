# Week 22 Day 2 Agenda

1. `git pull`
1. Check-in to bootcampspot
1. Week 21 Mini Project: Tech Matchup
1. Use `create-react-app` to create a `00-practice-app` folder in the `01-Activities` folder
1. Global State: React Context API
1. Week 22 Activity 01: Introducing Context Providers
1. Week 22 Activity 02: Modify the Student Context
1. Review Activity 02
1. Week 22 Activity 03: Introducing Context Consumers
1. Week 22 Activity 04: Consume the `StudentContext` using the custom `useStudentContext` hook
1. Review Activity 04
