# Week 22 Day 2 Agenda

1. `git pull`
1. Check-in to bootcampspot
1. Review global state: React Context API
1. Week 22 Activity 05: Introducing state **reducers**
1. Week 22 Activity 06: Make the tests pass
1. Review Activity 06
1. Week 22 Activity 07: What are **actions**
1. Skip 22 Activity 08
1. Week 22 Activity 09: `useReducer` hook
1. Week 22 Activity 10: Implement `useReducer` hook for the student context
1. Review 22 Activity 10
1. Week 22 Activity 25: Review Actions and Reducers
1. Week 22 Activity 26: Analyze the Code
1. Week 22 Activity 28: Mini-Project - Refactor Context API to use Redux
