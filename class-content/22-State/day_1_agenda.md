# Week 22 Day 1 Agenda

1. `git pull`
1. Check-in to bootcampspot
1. Week 21 Activity 25: Use the JWT for authentication (resolver `context`)
1. Week 21 Activity 26: Debug the resolver context
1. Review Activity 26
1. Week 20 Mini Project: Bucket List
