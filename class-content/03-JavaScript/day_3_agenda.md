# Agenda

1. Homework 2 due today!
1. `git pull`
1. Review: Array, Iteration, Functions, Scope
1. Week 3 Activity 21: what is an object?
1. Week 3 Activity 22: create a drink object (10 min)
1. Review Activity 22
1. Week 3 Activity 19: What is a method? - intro to Array methods
1. Week 3 Activity 20: Planet arrays (10 min)
1. Review Activity 20
1. Week 3 Activity 23: What is an object method? - make your own
1. Week 3 Activity 24: debug the pet shelter messages (15 min)
1. Review Activity 24
1. Week 3 Activity 25: Object `this`
1. Week 3 Activity 26: Comment the code (12 mins)
1. Review Activity 26
1. Week 3 Activity 27: Git Branch (20 mins)
1. Week 3 Activity 28: Mini Project - Rock Paper Scissors
1. Homework 3: Password Generator
