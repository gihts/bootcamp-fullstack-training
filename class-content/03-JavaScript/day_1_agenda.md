# Agenda

1. Homework
1. Slides: Javascript
1. Look ahead: Rock Paper Scissors
1. Week 3 Activity 01: where do we write javascript?
1. Week 3 Activity 02: include an external JS file (15 min)
1. Review Activity 02
1. Week 3 Activity 03: what is a variable?
1. Week 3 Activity 04: create some variables (15 min)
1. Review Activity 04
1. Week 3 Activity 05: what is a variable type?
1. Week 3 Activity 06: predict what the console will print out (15 min)
1. Review Activity 06
1. Week 3 Activity 07: what is an operator?
1. Week 3 Activity 08: Use the correct logical operators (15 min)
1. Review Activity 08
1. Week 3 Activity 09: what is a conditional statement (if/else)?
1. Week 3 Activity 10: Control program flow with if/else and conditional operators (15 min)
1. Review Activity 10:
