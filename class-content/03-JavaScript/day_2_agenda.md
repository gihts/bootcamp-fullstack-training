# Agenda

1. `git pull` vs `git clone`
1. 4,294,967,295
1. Week 3 Activity 11: what is an array?
1. Week 3 Activity 12: create class list (15 min)
1. Review Activity 12
1. Week 3 Activity 13: What are iteration and for loops?
1. Week 3 Activity 14: more class list (15 min)
1. Review Activity 14
1. Week 3 Activity 15: what are functions?
1. Week 3 Activity 16: create a function that checks for equality (15 min)
1. Review Activity 16
1. Week 3 Activity 17: Introduction to variable scope
1. Week 3 Activity 18: work with variable scope (15 min)
1. Review Activity 18
1. Week 3 Activity 19: what is a method?
1. Week 3 Activity 20: using array methods (15 min)
1. Review Activity 20
