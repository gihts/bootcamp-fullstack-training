# Pre-Read: Week 1

## Day 1

- [How (not) to learn](../../supplemental/howToLearn.md)
- [How to Set Up VS Code for Web Development in A Few Simple Steps](https://www-freecodecamp-org.cdn.ampproject.org/c/s/www.freecodecamp.org/news/how-to-set-up-vs-code-for-web-development/amp/)
- [How To Become A True Keyboard Warrior (And Stop Using Your Mouse)](https://levelup.gitconnected.com/how-to-become-a-true-keyboard-warrior-and-stop-using-your-mouse-a87cd29c5801)
- [Learn Enough Command Line to Be Dangerous](https://www.learnenough.com/command-line-tutorial/basics)
- [The Beginners Guide to Google-Fu? 10 tricks to be a Google-Fu Blackbelt|](https://medium.com/analytics-vidhya/https-medium-com-what-is-googlefu-tips-and-tricks-to-be-googlefu-advanced-powersearching-with-google-f7e5661a8bca)
- [An introduction to Git: what it is, and how to use it](https://www.freecodecamp.org/news/what-is-git-and-how-to-use-it-c341b049ae61/)

## Day 2

- [Responsive Web Design: Basic HTML and HTML 5](https://www.freecodecamp.org/learn/responsive-web-design/#basic-html-and-html5)
- [Responsive Web Design: Basic CSS](https://www.freecodecamp.org/learn/responsive-web-design/#basic-css)
- [Professional README Guide](https://coding-boot-camp.github.io/full-stack/github/professional-readme-guide)
- [Markdown Guide](https://www.markdownguide.org/getting-started/)
- [W3Schools HTML](https://www.w3schools.com/html/default.asp)
- [How to Use Semantic HTML5 to Structure Your Page Correctly](https://www.semrush.com/blog/semantic-html5-guide/)
- [HTML5 Semantic Tags: What Are They and How To Use Them!](https://www.semrush.com/blog/semantic-html5-guide/)
- [Quick Reminder About File Paths](https://css-tricks.com/quick-reminder-about-file-paths/)

## Day 3

- [Responsive Web Design: Applied Accessibility](https://www.freecodecamp.org/learn/responsive-web-design/#applied-accessibility)
- [W3Schools CSS](https://www.w3schools.com/css/default.asp)
- [Quick Reminder About File Paths](https://css-tricks.com/quick-reminder-about-file-paths/)
- [CSS Flow](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flow_Layout)
- [Normal Flow](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Normal_Flow)
- [Understanding the CSS box model for inline elements](https://hacks.mozilla.org/2015/03/understanding-inline-box-model/)
- [Learning Layout](http://learnlayout.com/no-layout.html)
- [CSS Tricks](https://css-tricks.com/)

### Extra

- [How web browsers work](https://hackernoon.com/how-do-web-browsers-work-40cefd2cb1e1)
- [Missing Semester of your CS Education](https://missing.csail.mit.edu/)
- [Customize Your Shell & Command Prompt](http://blog.taylormcgann.com/tag/prompt-color/)
- [Bash Academy](https://guide.bash.academy/)
- [Bash Guide for Beginners](http://www.tldp.org/LDP/Bash-Beginners-Guide/html/)
