# Agenda

## Link to Video

https://zoom.us/rec/play/Ilk8hwElBxunwUTIDU1xgF2mYJg6XEqC3tTRcCzIwyD5FIzobd_u8cQJb_dlJUA6hvyNendhRE1FO3Rh.I2RMQ78BmirfJVN_?startTime=1616114772000

## Agenda Items

1. Announcements: how to turn in the Pre Work
1. Installed Open in Default Browser Extension?
1. Slides: Computational thinking
1. What are User Stories?
1. README's
1. **Activity 2** - Create Directory and File - 5 minutes
1. Review Activity 2 - 5 minutes
1. **Activity 4** - Create, Clone and Push changes to a Github repo - 10 minutes
1. Review Activity 4 - 5 minutes
1. **Activity 5** - what is html? what are html elements?
1. **Activity 6** - create a web page with html - **15 minutes**
1. Review Activity 6
1. **Activity 7** - what are html attributes? what is a Stylesheet?
1. **Activity 8** - add some attributes - **15 minutes**
1. Review Activity 8
1. **Homework: Semantic Refactor**
