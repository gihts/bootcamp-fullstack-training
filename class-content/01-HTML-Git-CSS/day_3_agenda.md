# Agenda

## Link to Video

https://zoom.us/rec/share/LnoByy4-ZQqnb6a1lS0s6MpwvgKBi74LYc0kp-s92QJLQx_PwORueAQBEUmDr7Wf.DrqNSm_OqGG5i3Wi?startTime=1616257000000

## Agenda Items

1. `git pull`
1. [HTML and CSS Fundamentals Practice](https://www.freecodecamp.org/learn/responsive-web-design/#basic-html-and-html5)
1. How to create a github page
1. **Activity 9** - what is css? how do we change colors?
1. **Activity 10** - link a css file, add some color - **10 minutes**
1. Review Activity 10
1. **Activity 11** - how do we style fonts?
1. **Activity 12** - style some fonts - **10 minutes**
1. Review Activity 12
1. **Activity 13** - what are css selectors?
1. **Activity 14** - document the selector types **10 minutes**
1. Review Activity 14
1. **Activity 15** - HTML display
1. **Activity 16** - change the display on some elements **10 minutes**
1. Review Activity 16
1. **Activity 17** - what is the box model?
1. **Activity 18** - modify the padding, margin and border **10 minutes**
1. Review Activity 18
1. **Activity 19** - what is css positioning?
1. **Activity 20** - position some elements **15 minutes**
1. Review Activity 20
1. **Activity 21** - `git init` creating a local projects
1. **Activity 22** - Mini Project: build a whole page
