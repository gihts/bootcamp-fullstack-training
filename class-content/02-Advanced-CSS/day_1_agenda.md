# Agenda

1. Announcements: hw1 rubric
1. Week 1 Activity 22: Mini Project - 30 minutes
1. Review Mini Project
1. Slides: Advanced CSS
1. Week 2 Activity 01: what are media queries?
1. Week 2 Activity 02: apply media queries - 15 minutes
1. Review Activity 02
1. Week 2 Activity 03: what is flexbox?
1. Week 2 Activity 04: do some flexbox layout
1. Review Activity 04
1. Week 2 Activity 05: Responsive design
1. Week 2 Activity 06: Comment this code
1. Review Activity 06
