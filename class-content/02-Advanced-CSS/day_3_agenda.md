# Agenda

1. Week 2 Activity 19: Custom Forms
1. Week 2 Activity 20: Comment the form element css
1. Review Activity 20
1. Week 2 Activity 21: Wireframing
1. Week 2 Activity 22: Complete the wireframe
1. Review Activity 22
1. Week 2 Activity 23: More on CSS Selectors
1. Week 2 Activity 24: Apply advanced CSS selectors
1. Review Activity 24
1. Week 2 Activity 25: What are CSS Variables?
1. Week 2 Activity 26: use css variables to set a color theme
1. Review Activity 26
1. Week 2 Activity 28: Mini Project
1. Review Activity 28
1. Homework 2: Portfolio Page 1
