# Agenda

1. Fundamentals: [freeCodeCamp](https://www.freecodecamp.org/learn/responsive-web-design/)
1. Practice: [Flexbox Froggy](https://flexboxfroggy.com/)
1. Week 2 Activity 07: flexbox placement
1. Week 2 Activity 08: fix the product layout
1. Review Activity 08
1. Week 2 Activity 09: box styling with transforms
1. Week 2 Activity 10: style these boxes
1. Review Activity 10
1. Week 2 Activity 11: css resets
1. Week 2 Activity 12: correctly applying multiple css files
1. Review Activity 12
1. Week 2 Activity 13: typography
1. Week 2 Activity 14: style some text
1. Review Activity 14
1. Week 2 Activity 15: what are pseudo classes?
1. Week 2 Activity 16: make some stuff appear and disappear with hover
1. Review Activity 16
1. Week 2 Activity 17: what are pseudo elements?
1. Week 2 Activity 18: Let's create a tool tip
1. Review Activity 18
