# Agenda

1. No class on Saturday!
1. `git pull`
1. [Install Insomnia](https://insomnia.rest/download)
1. Slides: Intro to Node Servers
1. Week 11 Activity 1: First Server - node's `http` module
1. Week 11 Activity 2: Two Servers - servers and ports
1. Review Week 11 Activity 2
1. Week 11 Activity 3: Serving Portfolio HTML with Routing
1. Discuss Activity 3
1. Week 11 Activity 4: Serving HTML files
1. Week 11 Activity 5: Create a server with routes
1. Review Week 11 Activity 5
1. Week 11 Activity 6: HTTP Request Methods
