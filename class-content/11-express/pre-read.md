# Week 11 Pre Read

## Day 1

- [Port](https://developer.mozilla.org/en-US/docs/Glossary/Port)
- [How Internet Infrastructure Works - Ports and HTTP](https://computer.howstuffworks.com/internet/basics/internet-infrastructure10.htm)
- [An overview of HTTP](https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview)
- [Anatomy of a dynamic request](https://developer.mozilla.org/en-US/docs/Learn/Server-side/First_steps/Client-Server_overview#anatomy_of_a_dynamic_request)
- [Express JS](https://expressjs.com/)
- [Express, a popular Node.js Framework](https://flaviocopes.com/express/)

## Day 2

- [Express JS](https://expressjs.com/)
- [Express, a popular Node.js Framework](https://flaviocopes.com/express/)
