# Agenda

1. `git pull`
1. Slides: Intro to Express.js
1. Intro tonight's walkthrough: Star Wars App
1. Star Wars 1: Add some data, add aroute
1. Review Star Wars 1
1. Star Wars 2: what are `req.params`?
1. Review Star Wars 2
1. Star Wars 3: Analyze the code
1. Star Wars 4: Review the code analysis
1. Star Wars 5: Adding a POST route
1. Star Wars 5: what is req.body and how is it related to express.json()?
1. Star Wars 5: send a POST with Insomnia
1. Star Wars 6: res.sendfile: sending an HTML file
1. Star Wars 6: analyze view.html
1. Review Star Wars 6 view.html
1. FinalStarWarsApp: analyze add.html
1. Review FinalStarWarsApp add.html
